[![](https://jitpack.io/v/com.gitlab.flpdev/android_logger.svg)](https://jitpack.io/#com.gitlab.flpdev/android_logger)

# **Android Logger**

A lightweight android logging library

This library allows you to log all your app's procedures into a file for later reading and debugging.


## Installation

Add [JitPack](https://jitpack.io) to your `repositories` inside your build.gradle file:

```groovy
allprojects {
    repositories {
        // ...
        maven { url 'https://jitpack.io' }
    }
}
```

And the logger dependency itself:

```groovy
    implementation 'com.gitlab.flpdev:android_logger:1.0.0'
```

## Usage

* Init the Logger by callig Log.LogBuilder(), e.g:

```java
new Log.LogBuilder(this.getApplicationContext())
                .setFileExtension(".txt")
                .setFileName("myLogFileName")
                .setLogDateFormat("dd-MM-yyyy")
                .setIsDebug(true)
                .build();
```
**IF YOU TRY TO USE THE LOGGER WITHOUT CALLING THE BUILDER BEFORE YOU WILL GET AN EXCEPTION**

*You can change the default values by calling LogBuilder methods like in the example.*

* To write logs to the file simply call Log.d for debug level log, Log.e for error level log and log.w for warning level log

```java
    Log.d(TAG, "debug log", true);
    Log.w(TAG, "warning log", true);
    Log.e(TAG, "error log", true);
```

## Customization

* By default it will create a file per day with the current date as file name, e.g. **25_12_2020.log**. You can change the file name with the ***.setFileName()*** builder method.
* You can also change the default file extension (.log) to another one by calling the builder method ***.setFileExtension(".txt");***.
* The deafult log line format is:   **dd/MM/yyyy HH:mm:ss:SSS _LOG_TYPE_ TAG content**
                                    For example: **28/5/2020 20:10:12:312 _DEBUG_ MainActivity onCreate() was called**
* You also can change the date format used as file name if you choose to not override the file name. Just ***.callsetFileNameDateFormat(format)*** in the builder with your desired format.
* You can also see where the function that writes the call was called for better debug by calling ***Log.d(TAG, myContent, true);***.
* You can specify wether you want to also see the logs in the logcat panel in Android Studio. Call ***.setIsDebug(true/false)*** in the builder.

