package com.ferran.logger;

import android.content.Context;
import android.os.Looper;

import com.ferran.logger.exceptions.NoInitializedLogInstance;
import com.ferran.logger.utils.DateUtils;
import com.ferran.logger.utils.FileUtils;

import java.io.File;
import java.util.Calendar;
import java.util.WeakHashMap;

public class Log {

    private static final String LOG_DATE_FORMAT = "dd/MM/yyyy HH:mm:ss:SSS";
    private static final String LOG_FILE_NAME_DATE_FORMAT = "dd_MM_yyyy";

    private static final String DEBUG_TYPE = "_DEBUG_  ";
    private static final String WARNING_TYPE = "_WARNING_";
    private static final String ERROR_TYPE = "_ERROR_  ";
    private static final String BLANK_SPACE = " ";
    private String fileName = "";
    private String fileLocation = "";
    private String fileExtension = ""; //Default value
    private String logDateFormat;
    private String fileNameDateFormat = LOG_FILE_NAME_DATE_FORMAT;
    private static boolean isDebug = false;
    private static Log instance;

    /**
     * Writes a log line in the DEBUG channel
     * @param tag
     * @param data
     */
    public static void d(String tag, String data) {
        if (instance != null) {
            instance.writeToFile(DEBUG_TYPE, tag, data, false);
        } else {
            try {
                throw new NoInitializedLogInstance();
            } catch (NoInitializedLogInstance noInitializedLogInstance) {
                android.util.Log.d("Log.class", noInitializedLogInstance.getMessage());
            }
        }
    }

    /**
     * Writes a log line in the ERROR channel
     * @param tag
     * @param data
     */
    public static void e(String tag, String data) {
        if (instance != null) {
            instance.writeToFile(ERROR_TYPE, tag, data, false);
        } else {
            try {
                throw new NoInitializedLogInstance();
            } catch (NoInitializedLogInstance noInitializedLogInstance) {
                android.util.Log.d("Log.class", noInitializedLogInstance.getMessage());
            }
        }
    }

    /**
     * Writes a log line in the WARNING channel
     * @param tag
     * @param data
     */
    public static void w(String tag, String data) {
        if (instance != null) {
            instance.writeToFile(WARNING_TYPE, tag, data, false);
        } else {
            try {
                throw new NoInitializedLogInstance();
            } catch (NoInitializedLogInstance noInitializedLogInstance) {
                android.util.Log.d("Log.class", noInitializedLogInstance.getMessage());
            }
        }
    }

    /**
     * Writes a log line in the DEBUG channel
     * @param tag
     * @param data
     * @param showFrom adds last stacktrace to log line
     */
    public static void d(String tag, String data, boolean showFrom) {
        if (instance != null) {
            instance.writeToFile(DEBUG_TYPE, tag, data, showFrom);
        } else {
            try {
                throw new NoInitializedLogInstance();
            } catch (NoInitializedLogInstance noInitializedLogInstance) {
                android.util.Log.d("Log.class", noInitializedLogInstance.getMessage());
            }
        }
    }

    /**
     * Writes a log line in the ERROR channel
     * @param tag
     * @param data
     * @param showFrom adds last stacktrace to log line
     */
    public static void e(String tag, String data, boolean showFrom) {
        if (instance != null) {
            instance.writeToFile(ERROR_TYPE, tag, data, showFrom);
        } else {
            try {
                throw new NoInitializedLogInstance();
            } catch (NoInitializedLogInstance noInitializedLogInstance) {
                android.util.Log.d("Log.class", noInitializedLogInstance.getMessage());
            }
        }
    }

    /**
     * Writes a log line in the WARNING channel
     * @param tag
     * @param data
     * @param showFrom adds last stacktrace to log line
     */
    public static void w(String tag, String data, boolean showFrom) {
        if (instance != null) {
            instance.writeToFile(WARNING_TYPE, tag, data, showFrom);
        } else {
            try {
                throw new NoInitializedLogInstance();
            } catch (NoInitializedLogInstance noInitializedLogInstance) {
                android.util.Log.d("Log.class", noInitializedLogInstance.getMessage());
            }
        }
    }

    private void writeToFile(String type, String tag, String data, boolean showFrom) {
        if(isDebug) {
            writeToConsole(type, tag, data);
        }
        File f = new File(this.fileLocation + "/" + this.fileName + this.fileExtension);
        FileUtils.writeToFile(f, formatData(type, data, tag, showFrom), true);
    }

    private static void writeToConsole(String type, String tag, String data) {
        switch (type) {
            case DEBUG_TYPE:
                android.util.Log.d(tag, data);
                break;
            case ERROR_TYPE:
                android.util.Log.e(tag, data);
                break;
            case WARNING_TYPE:
                android.util.Log.w(tag, data);
        }
    }

    private String formatData(String logType, String content, String tag, boolean showFrom) {
        StringBuilder sb = new StringBuilder()
                .append(DateUtils.getLogFormatedDate(Calendar.getInstance().getTime(), logDateFormat))
                .append(BLANK_SPACE)
                .append(logType)
                .append(BLANK_SPACE)
                .append(tag + ":")
                .append(BLANK_SPACE)
                .append(content);

        if (showFrom) {
            String className = Looper.getMainLooper().getThread().getStackTrace()[5].getClassName();
            className = className.substring(className.lastIndexOf(".") + 1);
            String methodName = Looper.getMainLooper().getThread().getStackTrace()[5].getMethodName();
            sb.append(" from: ")
                    .append(className + "." + methodName);
        }
        sb.append("\n");

        return sb.toString();
    }

    //GETTERS AND SETTERS

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public boolean isDebug() {
        return isDebug;
    }

    public void setIsDebug(boolean isDebug) {
        this.isDebug = isDebug;
    }

    public void setLogDateFormat(String logDateFormat) {
        this.logDateFormat = logDateFormat;
    }

    public void setFileNameDateFormat(String fileNameDateFormat) {
        this.fileNameDateFormat = fileNameDateFormat;
    }

    // BUILDER PATTERN IMPLEMENTATION
    public static class LogBuilder {

        private String fileLocation;
        private String fileName;
        private String fileExtension;
        private boolean isDebug;
        private String logDateFormat;
        private String fileNameDateFormat;

        public LogBuilder(Context context) {
            //Default values
            logDateFormat = LOG_DATE_FORMAT;
            fileNameDateFormat = LOG_FILE_NAME_DATE_FORMAT;
            this.fileLocation = context.getExternalFilesDir(null).getAbsolutePath();
            this.fileName = DateUtils.getCurrentDay(fileNameDateFormat);
            this.fileExtension = ".log";
            this.isDebug = false;

        }

        /**
         * Sets the log file location, by default it's stored in the app private folder under Android/data
         * @param fileLocation
         * @return
         */
        public LogBuilder setFileLocation(String fileLocation){
            this.fileLocation = fileLocation;
            return this;
        }

        /**
         * Changes the default fileName. By default the name will be the current date, e.g 25/12/2020
         * @param fileName
         * @return
         */
        public LogBuilder setFileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        /**
         * Changes the default file extension. By default the extension is .log
         * You must add the dot to the extension (e.g .txt), otherwise it won't work
         * @param fileExtension
         * @return
         */
        public LogBuilder setFileExtension(String fileExtension) {
            this.fileExtension = fileExtension;
            return this;
        }

        /**
         * Sets isDebug to true or false. If isDebug is true you will se the logs in the android logcat window too
         * @param isDebug
         * @return
         */
        public LogBuilder setIsDebug(boolean isDebug) {
            this.isDebug = isDebug;
            return this;
        }

        /**
         * Sets the LogDateFormat that's displayed in front of every log line. By default the format is dd/MM/yyyy HH:mm:ss:SSS, e.g. 12/25/2020 12:10:35:125
         * @param logDateFormat
         * @return
         */
        public LogBuilder setLogDateFormat(String logDateFormat) {
            this.logDateFormat = logDateFormat;
            return this;
        }

        /**
         * Sets the fileNameDateFormat. If fileName it's been kept by default, you can change the dateformat that will be the file's name calling this function
         * @param fileNameDateFormat
         * @return
         */
        public LogBuilder setFileNameDateFormat(String fileNameDateFormat) {
            this.fileNameDateFormat = fileNameDateFormat;
            return this;
        }

        /**
         * Builds the logger with specified parameters. It won't return any instance since it is a Singleton, but instead will create a new instance internally.
         * @return void
         */
        public void build() {
            instance = new Log();
            instance.setFileExtension(this.fileExtension);
            instance.setFileName(this.fileName);
            instance.setFileLocation(this.fileLocation);
            instance.setIsDebug(this.isDebug);
            instance.setFileNameDateFormat(this.fileNameDateFormat);
            instance.setLogDateFormat(this.logDateFormat);
        }
    }
}
