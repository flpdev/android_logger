package com.ferran.logger.utils;

import java.io.File;
import java.io.FileWriter;

public abstract class FileUtils {

    public static boolean writeToFile(File f, String data, boolean append) {
        boolean result = false;
        FileWriter fw = null;
        try {
            fw = new FileWriter(f.getAbsolutePath(), append);
            fw.write(data);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            try {
                if (fw != null) {
                    fw.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
