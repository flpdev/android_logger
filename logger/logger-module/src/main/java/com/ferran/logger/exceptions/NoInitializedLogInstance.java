package com.ferran.logger.exceptions;

public class NoInitializedLogInstance extends Exception {
    private static final String message = "No instance of Log found. Have you called Log.LogBuilder()?";

    public NoInitializedLogInstance() {
        super(message);
    }
}
